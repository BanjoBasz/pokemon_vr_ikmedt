AFRAME.registerComponent('reloader', {
  schema: {},
  init: function () {
    const pokemons = document.querySelectorAll("[pokémon]");
    this.newPokemon = function(){
      for(let i = 0; i < pokemons.length; i++){
        let randomNum = Math.floor(Math.random() * 800) + 1;
        const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
        fetch(BASE_URL + randomNum)
          	.then(response => response.json())
            .then(data => pokemons[i].setAttribute("src",data.sprites.front_default));
      }
    }
    this.el.addEventListener("mouseenter", this.newPokemon);
    this.newPokemon();
  },
  update: function () {},
  tick: function () {},
  remove: function () {},
  pause: function () {},
  play: function () {}
});
